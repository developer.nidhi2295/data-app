import {openDatabase} from 'react-native-sqlite-storage';

export let db = openDatabase(
  {name: 'UserDatabase.db', createFromLocation: 1},
  () => console.log('Database Opened!'),
  error => console.log('Database Error', error),
);

export const checkForTable = () => {
  return new Promise((resolve, reject) => {
    db.transaction(function (txn) {
      txn.executeSql(
        "SELECT name FROM sqlite_master WHERE type='table' AND name='user_data'",
        [],
        function (tx, res) {
          if (res.rows.length == 0) {
            txn.executeSql('DROP TABLE IF EXISTS user_data', []);
            txn.executeSql(
              `CREATE TABLE IF NOT EXISTS user_data(
              id INTEGER PRIMARY KEY AUTOINCREMENT, 
              name VARCHAR(50), 
              phone VARCHAR(15), 
              user_id VARCHAR(30),
              address VARCHAR(255),
              dob VARCHAR(15),
              treatment_price VARCHAR(10),
              review_price VARCHAR(10),
              date1 VARCHAR(15),
              date2 VARCHAR(15),
              allergy_test VARCHAR(225),
              treatment_date VARCHAR(15),
              treatment_zone VARCHAR(100),
              treatment_technique VARCHAR(100),
              treatment_color VARCHAR(50),
              treatment_needle VARCHAR(50),
              retouch_date1 VARCHAR(15),
              retouch_zone1 VARCHAR(100),
              retouch_technique1 VARCHAR(100),
              retouch_color1 VARCHAR(50),
              retouch_needle1 VARCHAR(50),
              retouch_date2 VARCHAR(15),
              retouch_zone2 VARCHAR(100),
              retouch_technique2 VARCHAR(100),
              retouch_color2 VARCHAR(50),
              retouch_needle2 VARCHAR(50),
              retouchDescription VARCHAR(255),
              observation VARCHAR(355),
              previous_micro VARCHAR(355),
              que1 VARCHAR(355),
              que2 VARCHAR(355),
              que3 VARCHAR(355),
              que4 VARCHAR(355),
              que5 VARCHAR(355),
              que6 VARCHAR(355),
              que7 VARCHAR(355),
              que8 VARCHAR(355),
              que9 VARCHAR(355),
              que10 VARCHAR(355),
              que11 VARCHAR(355),
              que12 VARCHAR(355),
              que13 VARCHAR(355),
              que14 VARCHAR(355),
              que15 VARCHAR(355),
              consent_name VARCHAR(50),
              consent_date1 VARCHAR(15),
              zone1 VARCHAR(100),
              consent_date2 VARCHAR(15),
              zone2 VARCHAR(100),
              date3 VARCHAR(15),
              zone3 VARCHAR(100),
              center_name VARCHAR(100),
              from_user VARCHAR(100),
              signdate VARCHAR(15)
              )
            `,
              [],
            );
          }
          console.log('-----> Table Creation Done');
          resolve(true);
        },
      );
    });
  });
};

export const getAllData = () => {
  return new Promise((resolve, reject) => {
    console.log('===> Fetching data');
    db.transaction(txn => {
      txn.executeSql('SELECT * FROM user_data', [], (tx, results) => {
        var temp = [];
        for (let i = 0; i < results.rows.length; ++i)
          temp.push(results.rows.item(i));
        console.log('Temp = ', temp);
        resolve(temp);
        return temp;
      });
    });
    console.log('=====> Data Fetch Done');
  });
};

export const saveData = async data => {
  const {personalInfo, treatmentInfo, retouchInfo, medicalInfo, consentInfo} =
    data;
  return new Promise((resolve, reject) => {
    db.transaction(function (tx) {
      console.log('Saving Data ==== ');
      tx.executeSql(
        `INSERT INTO user_data(
        name,
        phone,
        user_id,
        address,
        dob,
        treatment_price,
        review_price,
        date1,
        date2,
        allergy_test,
        treatment_date,
        treatment_zone,
        treatment_technique,
        treatment_color,
        treatment_needle,
        retouch_date1,
        retouch_zone1,
        retouch_technique1,
        retouch_color1,
        retouch_needle1,
        retouch_date2,
        retouch_zone2,
        retouch_technique2,
        retouch_color2,
        retouch_needle2,
        retouchDescription,
        observation,
        previous_micro,
        que1,
        que2,
        que3,
        que4,
        que5,
        que6,
        que7,
        que8,
        que9,
        que10,
        que11,
        que12,
        que13,
        que14,
        que15,
        consent_name,
        consent_date1,
        zone1,
        consent_date2,
        zone2,
        date3,
        zone3,
        center_name,
        from_user,
        signdate
        ) VALUES (
        ?,?,?,?,?,?,?,?,?,?,
        ?,?,?,?,?,?,?,?,?,?,
        ?,?,?,?,?,?,?,?,?,?,
        ?,?,?,?,?,?,?,?,?,?,
        ?,?,?,?,?,?,?,?,?,?,
        ?,?,?
        )`,
        [
          personalInfo.name,
          personalInfo.phone,
          personalInfo.id,
          personalInfo.address,
          personalInfo.dob,
          personalInfo.treatment_price,
          personalInfo.review_price,
          personalInfo.date1,
          personalInfo.date2,
          personalInfo.allergy_test,
          treatmentInfo.date,
          treatmentInfo.zone,
          treatmentInfo.technique,
          treatmentInfo.color,
          treatmentInfo.needle,
          retouchInfo.retouch1.date,
          retouchInfo.retouch1.zone,
          retouchInfo.retouch1.technique,
          retouchInfo.retouch1.color,
          retouchInfo.retouch1.needle,
          retouchInfo.retouch2.date,
          retouchInfo.retouch2.zone,
          retouchInfo.retouch2.technique,
          retouchInfo.retouch2.color,
          retouchInfo.retouch2.needle,
          retouchInfo.retouchDescription,
          retouchInfo.observation,
          retouchInfo.previous_micro,
          medicalInfo.que1,
          medicalInfo.que2,
          medicalInfo.que3,
          medicalInfo.que4,
          medicalInfo.que5,
          medicalInfo.que6,
          medicalInfo.que7,
          medicalInfo.que8,
          medicalInfo.que9,
          medicalInfo.que10,
          medicalInfo.que11,
          medicalInfo.que12,
          medicalInfo.que13,
          medicalInfo.que14,
          medicalInfo.que15,
          consentInfo.name,
          consentInfo.date1,
          consentInfo.zone1,
          consentInfo.date2,
          consentInfo.zone2,
          consentInfo.date3,
          consentInfo.zone3,
          consentInfo.center_name,
          consentInfo.from,
          consentInfo.signdate,
        ],
        (tx, results) => {
          console.log('Results', results.rowsAffected);
          if (results.rowsAffected > 0) {
            resolve('Record saved Successfully');
          } else resolve('Registration Failed');
        },
      );
      console.log('Data Saved ==== ');
    });
  });
};

export const deleteRecord = async id => {
  return new Promise((resolve, reject) => {
    db.transaction(tx => {
      tx.executeSql(
        'DELETE FROM  user_data where id=?',
        [id],
        (tx, results) => {
          console.log('Results', results.rowsAffected);
          if (results.rowsAffected > 0) {
            resolve('User deleted successfully');
          } else {
            resolve('Please insert a valid User Id');
          }
        },
      );
    });
  });
};

export const updateRecord = async (userTableId, data) => {
  const {personalInfo, treatmentInfo, retouchInfo, medicalInfo, consentInfo} =
    data;
  return new Promise((resolve, reject) => {
    db.transaction(tx => {
      tx.executeSql(
        `UPDATE user_data set
        name=?,
        phone=?,
        user_id=?,
        address=?,
        dob=?,
        treatment_price=?,
        review_price=?,
        date1=?,
        date2=?,
        allergy_test=?,
        treatment_date=?,
        treatment_zone=?,
        treatment_technique=?,
        treatment_color=?,
        treatment_needle=?,
        retouch_date1=?,
        retouch_zone1=?,
        retouch_technique1=?,
        retouch_color1=?,
        retouch_needle1=?,
        retouch_date2=?,
        retouch_zone2=?,
        retouch_technique2=?,
        retouch_color2=?,
        retouch_needle2=?,
        retouchDescription=?,
        observation=?,
        previous_micro=?,
        que1=?,
        que2=?,
        que3=?,
        que4=?,
        que5=?,
        que6=?,
        que7=?,
        que8=?,
        que9=?,
        que10=?,
        que11=?,
        que12=?,
        que13=?,
        que14=?,
        que15=?,
        consent_name=?,
        consent_date1=?,
        zone1=?,
        consent_date2=?,
        zone2=?,
        date3=?,
        zone3=?,
        center_name=?,
        from_user=?,
        signdate=?
        where id=?`,
        [
          personalInfo.name,
          personalInfo.phone,
          personalInfo.id,
          personalInfo.address,
          personalInfo.dob,
          personalInfo.treatment_price,
          personalInfo.review_price,
          personalInfo.date1,
          personalInfo.date2,
          personalInfo.allergy_test,
          treatmentInfo.date,
          treatmentInfo.zone,
          treatmentInfo.technique,
          treatmentInfo.color,
          treatmentInfo.needle,
          retouchInfo.retouch1.date,
          retouchInfo.retouch1.zone,
          retouchInfo.retouch1.technique,
          retouchInfo.retouch1.color,
          retouchInfo.retouch1.needle,
          retouchInfo.retouch2.date,
          retouchInfo.retouch2.zone,
          retouchInfo.retouch2.technique,
          retouchInfo.retouch2.color,
          retouchInfo.retouch2.needle,
          retouchInfo.retouchDescription,
          retouchInfo.observation,
          retouchInfo.previous_micro,
          medicalInfo.que1,
          medicalInfo.que2,
          medicalInfo.que3,
          medicalInfo.que4,
          medicalInfo.que5,
          medicalInfo.que6,
          medicalInfo.que7,
          medicalInfo.que8,
          medicalInfo.que9,
          medicalInfo.que10,
          medicalInfo.que11,
          medicalInfo.que12,
          medicalInfo.que13,
          medicalInfo.que14,
          medicalInfo.que15,
          consentInfo.name,
          consentInfo.date1,
          consentInfo.zone1,
          consentInfo.date2,
          consentInfo.zone2,
          consentInfo.date3,
          consentInfo.zone3,
          consentInfo.center_name,
          consentInfo.from,
          consentInfo.signdate,
          userTableId,
        ],
        (tx, results) => {
          console.log('Results', results.rowsAffected);
          if (results.rowsAffected > 0) {
            resolve('User Record updated successfully');
          } else resolve('Updation Failed');
        },
      );
    });
  });
};
