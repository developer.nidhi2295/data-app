import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Colors from '../constants/Colors';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from '../utils/responsive-screen';

const UserCard = props => {
  return (
    <View style={styles.container}>
      <View style={styles.detailBox}>
        <Text style={styles.mainText}>{props.name}</Text>
        <Text style={styles.subText}>{props.id}</Text>
      </View>
      <Icon
        name="pencil"
        size={hp(3)}
        color={Colors.greyText}
        style={{marginRight: 5}}
        onPress={props.onEdit}
      />
      <Ionicons
        name="trash"
        size={hp(3)}
        color={Colors.themeRed}
        onPress={props.onDelete}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.white,
    borderRadius: 5,
    padding: wp(3),
    flexDirection: 'row',
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
    marginVertical: hp(0.8),
    marginHorizontal: wp(2),
    borderLeftWidth: wp(1.2),
  },
  detailBox: {
    width: '80%',
    marginHorizontal: wp(1.5),
  },
  mainText: {
    fontSize: 16,
    fontFamily: 'Poppins-Medium',
  },
  subText: {
    fontSize: 11,
    fontFamily: 'Poppins-Light',
    color: Colors.greyText,
  },
});

export default UserCard;
