import React, {useState} from 'react';
import {View, TextInput, StyleSheet, Platform} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from '../utils/responsive-screen';
import Colors from '../constants/Colors';
import Icon from 'react-native-vector-icons/Ionicons';

const SearchBox = props => {
  const [isFocused, setFocus] = useState(false);

  return (
    <View style={styles.inputContainer}>
      <Icon
        name={'search'}
        size={22}
        style={styles.searchIcon}
        color={Colors.greyText}
      />
      <TextInput
        style={styles.inputBox}
        placeholderTextColor={Colors.greyText}
        onFocus={() => setFocus(!isFocused)}
        onBlur={() => setFocus(!isFocused)}
        placeholder={'Registro de búsqueda'}
        {...props}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  inputContainer: {
    width: wp(92),
    borderRadius: 5,
    flexDirection: 'row',
    alignSelf: 'center',
    backgroundColor: Colors.white,
    alignItems: 'center',
    shadowOpacity: 0.9,
    shadowColor: Colors.lightText,
    shadowOffset: {top: 1, bottom: 1},
    shadowOpacity: 0.5,
    shadowRadius: 2,
    elevation: 6,
    marginVertical: hp(1.5)
  },
  inputBox: {
    height: '100%',
    fontSize: 16,
    fontFamily: 'Poppins-Regular',
    color: Colors.greyText,
    paddingLeft: wp(2),
    flex: 1,
    paddingVertical: hp(1),
    alignItems: 'center',
  },
  searchIcon: {
    alignSelf: 'center',
    marginLeft: wp(3),
    marginVertical: hp(1)
  },
});

export default SearchBox;
