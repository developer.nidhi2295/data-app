import React from 'react';
import {StyleSheet, Text, TextInput, View} from 'react-native';
import Colors from '../constants/Colors';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from '../utils/responsive-screen';

const QuestionBox = props => {
  return (
    <View>
      <Text style={styles.question}>{props?.question}</Text>
      <TextInput
        style={styles.inputBox}
        autoCorrect={false}
        multiline={true}
        numberOfLines={4}
        placeholder="Respuesta"
        {...props}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  question: {
    fontFamily: 'Poppins-Medium',
    fontSize: 16,
    color: Colors.themeGrey,
  },
  inputBox: {
    height: heightPercentageToDP(12),
    borderRadius: 5,
    marginVertical: heightPercentageToDP(1),
    fontFamily: 'Poppins-Regular',
    color: Colors.black,
    fontSize: 16,
    paddingVertical: heightPercentageToDP(1),
    paddingHorizontal: widthPercentageToDP(2),
    borderColor: Colors.themeGrey,
    borderWidth: 1,
  },
});

export default QuestionBox;
