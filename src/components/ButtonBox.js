import React from 'react';
import {
  ActivityIndicator,
  StyleSheet,
  Text,
  TouchableOpacity,
} from 'react-native';
import Colors from '../constants/Colors';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from '../utils/responsive-screen';

const ButtonBox = props => {
  return (
    <TouchableOpacity
      style={{...styles.container, ...props.style}}
      activeOpacity={0.7}
      onPress={props.onPress}>
      <Text style={{...styles.buttonText, ...props.titleStyle}}>
        {props.title}
      </Text>
      {props.isLoading && (
        <ActivityIndicator color={Colors.themePurple} size={'small'} />
      )}
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingVertical: hp(1.2),
    paddingHorizontal: wp(10),
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.themePurple,
    borderRadius: 5,
    flexDirection: 'row',
  },
  buttonText: {
    fontSize: 16,
    color: Colors.white,
    fontFamily: 'Poppins-Medium',
  },
});

export default ButtonBox;
