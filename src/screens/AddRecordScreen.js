import React, {useState, createRef, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  ScrollView,
  KeyboardAvoidingView,
  TextInput,
  Modal,
  TouchableOpacity,
  Image,
} from 'react-native';
import AnimatedTextBox from '../components/AnimatedTextBox';
import ButtonBox from '../components/ButtonBox';
import QuestionBox from '../components/QuestionBox';
import Colors from '../constants/Colors';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from '../utils/responsive-screen';
import SignatureCapture from 'react-native-signature-capture';
import Icon from 'react-native-vector-icons/AntDesign';
import {saveData, updateRecord} from '../database/user-operations';
import {saveDataToFile} from '../utils/save-pdf';
import DatePicker from 'react-native-date-picker';
import moment from 'moment';

const AddRecordScreen = props => {
  const {userData} = props.route?.params ? props.route.params : '';

  const [currentForm, setForm] = useState(0);
  const [modalVisible, setModalVisible] = useState(false);
  const [loading, setLoading] = useState(false);
  const [personalInfo, setPersonalInfo] = useState({
    name: '',
    phone: '',
    address: '',
    id: '',
    dob: '',
    treatment_price: '',
    review_price: '',
    date1: '',
    date2: '',
    allergy_test: '',
  });
  const [treatmentInfo, setTreatmentInfo] = useState({
    date: '',
    zone: '',
    technique: '',
    color: '',
    needle: '',
  });
  const [retouchInfo, setRetouchInfo] = useState({
    retouch1: {
      date: '',
      zone: '',
      technique: '',
      color: '',
      needle: '',
    },
    retouch2: {
      date: '',
      zone: '',
      color: '',
      technique: '',
      needle: '',
    },
    retouchDescription: '',
    observation: '',
    previous_micro: '',
  });
  const [medicalInfo, setMedicalInfo] = useState({
    que1: '',
    que2: '',
    que3: '',
    que4: '',
    que5: '',
    que6: '',
    que7: '',
    que8: '',
    que9: '',
    que10: '',
    que11: '',
    que12: '',
    que13: '',
    que14: '',
    que15: '',
  });
  const [consentInfo, setConsetInfo] = useState({
    name: '',
    id: '',
    dob: '',
    zone1: '',
    date1: '',
    zone2: '',
    date2: '',
    zone3: '',
    date3: '',
    center_name: '',
    from: '',
    signdate: '',
  });
  const [signature, setSignature] = useState();
  const [open, setOpen] = useState({
    dob: false,
    date1: false,
    date2: false,
    treatmentDate: false,
    retouchDate1: false,
    retouchDate2: false,
    consetDob: false,
    consetDate1: false,
    consetDate2: false,
    consetDate3: false,
    consentDate4: false,
  });
  const sign = createRef();

  const saveSign = () => {
    sign.current.saveImage();
  };

  const resetSign = () => {
    sign.current.resetImage();
  };

  const _onSaveEvent = result => {
    alert('Ha firmado correctamente');
    setSignature('data:image/png;base64,' + result.encoded);
    console.log(result);
    setModalVisible(!modalVisible);
  };

  useEffect(() => {
    if (userData) {
      setPersonalInfo({
        ...personalInfo,
        name: userData.name,
        phone: userData.phone,
        address: userData.address,
        id: userData.user_id,
        dob: userData.dob,
        treatment_price: userData.treatment_price,
        review_price: userData.review_price,
        date1: userData.date1,
        date2: userData.date2,
        allergy_test: userData.allergy_test,
      });
      setTreatmentInfo({
        ...treatmentInfo,
        date: userData.treatment_date,
        zone: userData.treatment_zone,
        technique: userData.treatment_technique,
        color: userData.treatment_color,
        needle: userData.treatment_needle,
      });
      setRetouchInfo({
        ...setRetouchInfo,
        retouch1: {
          date: userData.retouch_date1,
          zone: userData.retouch_zone1,
          technique: userData.retouch_technique1,
          color: userData.retouch_color1,
          needle: userData.retouch_needle1,
        },
        retouch2: {
          date: userData.retouch_date2,
          zone: userData.retouch_zone2,
          technique: userData.retouch_technique2,
          color: userData.retouch_color2,
          needle: userData.retouch_needle2,
        },
        retouchDescription: userData.retouchDescription,
        observation: userData.observation,
        previous_micro: userData.previous_micro,
      });
      setMedicalInfo({
        ...medicalInfo,
        que1: userData.que1,
        que2: userData.que2,
        que3: userData.que3,
        que4: userData.que4,
        que5: userData.que5,
        que6: userData.que6,
        que7: userData.que7,
        que8: userData.que8,
        que9: userData.que9,
        que10: userData.que10,
        que11: userData.que11,
        que12: userData.que12,
        que13: userData.que13,
        que14: userData.que14,
        que15: userData.que15,
      });
      setConsetInfo({
        ...consentInfo,
        name: userData.consent_name,
        id: userData.user_id,
        dob: userData.dob,
        zone1: userData.zone1,
        date1: userData.consent_date1,
        zone2: userData.zone2,
        date2: userData.consent_date2,
        zone3: userData.zone3,
        date3: userData.date3,
        center_name: userData.center_name,
        from: userData.from_user,
        signdate: userData.signdate,
      });
    }
  }, [userData]);

  const fieldData = [
    {
      id: 1,
      title: 'DATOS CLIENTE',
      details: (
        <View>
          <AnimatedTextBox
            label={'Nombre'}
            style={styles.inputBox}
            value={personalInfo.name}
            onChangeText={text =>
              setPersonalInfo({...personalInfo, name: text})
            }
            autoCapitalize="sentences"
          />
          <AnimatedTextBox
            label={'Teléfonos'}
            style={styles.inputBox}
            value={personalInfo.phone}
            onChangeText={text =>
              setPersonalInfo({...personalInfo, phone: text})
            }
            autoCapitalize="sentences"
            keyboardType="phone-pad"
          />
          <AnimatedTextBox
            label={'Dirección'}
            style={styles.inputBox}
            value={personalInfo.address}
            onChangeText={text =>
              setPersonalInfo({...personalInfo, address: text})
            }
            autoCapitalize="sentences"
          />
          <AnimatedTextBox
            label={'DNI'}
            style={styles.inputBox}
            value={personalInfo.id}
            onChangeText={text => setPersonalInfo({...personalInfo, id: text})}
            autoCapitalize="sentences"
          />
          <TouchableOpacity
            onPress={() => setOpen({...open, dob: !open.dob})}
            activeOpacity={0.7}
            style={styles.dateInputBox}>
            <Text style={styles.dateText}>
              {personalInfo.dob ? personalInfo.dob : 'Fecha de Nacimiento'}
            </Text>
          </TouchableOpacity>
          <AnimatedTextBox
            label={'Precio del Tratamiento'}
            style={styles.inputBox}
            value={personalInfo.treatment_price}
            onChangeText={text =>
              setPersonalInfo({...personalInfo, treatment_price: text})
            }
            autoCapitalize="sentences"
            keyboardType="numeric"
          />
          <AnimatedTextBox
            label={'Precio del Repaso'}
            style={styles.inputBox}
            value={personalInfo.review_price}
            onChangeText={text =>
              setPersonalInfo({...personalInfo, review_price: text})
            }
            autoCapitalize="sentences"
            keyboardType="numeric"
          />
          <TouchableOpacity
            onPress={() => setOpen({...open, date1: !open.date1})}
            activeOpacity={0.7}
            style={styles.dateInputBox}>
            <Text style={styles.dateText}>
              {personalInfo.date1 ? personalInfo.date1 : 'Fecha 1a Visita'}
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => setOpen({...open, date2: !open.date2})}
            activeOpacity={0.7}
            style={styles.dateInputBox}>
            <Text style={styles.dateText}>
              {personalInfo.date2 ? personalInfo.date2 : 'Fecha 2a Visita'}
            </Text>
          </TouchableOpacity>
          <AnimatedTextBox
            label={'Prueba Alérgia'}
            style={styles.inputBox}
            value={personalInfo.allergy_test}
            onChangeText={text =>
              setPersonalInfo({...personalInfo, allergy_test: text})
            }
            autoCapitalize="sentences"
          />
        </View>
      ),
    },
    {
      id: 2,
      title: 'TRATAMIENTOS REALIZADOS',
      details: (
        <View>
          <TouchableOpacity
            onPress={() =>
              setOpen({...open, treatmentDate: !open.treatmentDate})
            }
            activeOpacity={0.7}
            style={styles.dateInputBox}>
            <Text style={styles.dateText}>
              {treatmentInfo.date ? treatmentInfo.date : 'Fecha'}
            </Text>
          </TouchableOpacity>
          <AnimatedTextBox
            label={'Zona'}
            style={styles.inputBox}
            value={treatmentInfo.zone}
            onChangeText={text =>
              setTreatmentInfo({...treatmentInfo, zone: text})
            }
            autoCapitalize="sentences"
          />
          <AnimatedTextBox
            label={'Técnica'}
            style={styles.inputBox}
            value={treatmentInfo.technique}
            onChangeText={text =>
              setTreatmentInfo({...treatmentInfo, technique: text})
            }
            autoCapitalize="sentences"
          />
          <AnimatedTextBox
            label={'Color'}
            style={styles.inputBox}
            value={treatmentInfo.color}
            onChangeText={text =>
              setTreatmentInfo({...treatmentInfo, color: text})
            }
            autoCapitalize="sentences"
          />
          <AnimatedTextBox
            label={'Aguja'}
            style={styles.inputBox}
            value={treatmentInfo.needle}
            onChangeText={text =>
              setTreatmentInfo({...treatmentInfo, needle: text})
            }
            autoCapitalize="sentences"
          />
        </View>
      ),
    },
    {
      id: 3,
      title: 'RETOQUE',
      details: (
        <View>
          <Text style={styles.subTitle}>RETOQUE#1</Text>
          <TouchableOpacity
            onPress={() => setOpen({...open, retouchDate1: !open.retouchDate1})}
            activeOpacity={0.7}
            style={styles.dateInputBox}>
            <Text style={styles.dateText}>
              {retouchInfo.retouch1.date ? retouchInfo.retouch1.date : 'Fecha'}
            </Text>
          </TouchableOpacity>
          <AnimatedTextBox
            label={'Zona'}
            style={styles.inputBox}
            value={retouchInfo.retouch1.zone}
            onChangeText={text =>
              setRetouchInfo({
                ...retouchInfo,
                retouch1: {...retouchInfo.retouch1, zone: text},
              })
            }
            autoCapitalize="sentences"
          />
          <AnimatedTextBox
            label={'Técnica'}
            style={styles.inputBox}
            value={retouchInfo.retouch1.technique}
            onChangeText={text =>
              setRetouchInfo({
                ...retouchInfo,
                retouch1: {...retouchInfo.retouch1, technique: text},
              })
            }
            autoCapitalize="sentences"
          />
          <AnimatedTextBox
            label={'Color'}
            style={styles.inputBox}
            value={retouchInfo.retouch1.color}
            onChangeText={text =>
              setRetouchInfo({
                ...retouchInfo,
                retouch1: {...retouchInfo.retouch1, color: text},
              })
            }
            autoCapitalize="sentences"
          />
          <AnimatedTextBox
            label={'Aguja'}
            style={styles.inputBox}
            value={retouchInfo.retouch1.needle}
            onChangeText={text =>
              setRetouchInfo({
                ...retouchInfo,
                retouch1: {...retouchInfo.retouch1, needle: text},
              })
            }
            autoCapitalize="sentences"
          />
          <Text style={styles.subTitle}>RETOQUE#2</Text>
          <TouchableOpacity
            onPress={() => setOpen({...open, retouchDate2: !open.retouchDate2})}
            activeOpacity={0.7}
            style={styles.dateInputBox}>
            <Text style={styles.dateText}>
              {retouchInfo.retouch2.date ? retouchInfo.retouch2.date : 'Fecha'}
            </Text>
          </TouchableOpacity>
          <AnimatedTextBox
            label={'Zona'}
            style={styles.inputBox}
            value={retouchInfo.retouch2.zone}
            onChangeText={text =>
              setRetouchInfo({
                ...retouchInfo,
                retouch2: {...retouchInfo.retouch2, zone: text},
              })
            }
            autoCapitalize="sentences"
          />
          <AnimatedTextBox
            label={'Técnica'}
            style={styles.inputBox}
            value={retouchInfo.retouch2.color}
            onChangeText={text =>
              setRetouchInfo({
                ...retouchInfo,
                retouch2: {...retouchInfo.retouch2, color: text},
              })
            }
            autoCapitalize="sentences"
          />
          <AnimatedTextBox
            label={'Color'}
            style={styles.inputBox}
            value={retouchInfo.retouch2.color}
            onChangeText={text =>
              setRetouchInfo({...retouchInfo.retouch2, color: text})
            }
            autoCapitalize="sentences"
          />
          <AnimatedTextBox
            label={'Aguja'}
            style={styles.inputBox}
            value={retouchInfo.retouch2.needle}
            onChangeText={text =>
              setRetouchInfo({
                ...retouchInfo,
                retouch2: {...retouchInfo.retouch2, needle: text},
              })
            }
            autoCapitalize="sentences"
          />
          <QuestionBox
            question={'RETOQUE'}
            value={retouchInfo.retouchDescription}
            onChangeText={text =>
              setRetouchInfo({
                ...retouchInfo,
                retouchDescription: text,
              })
            }
            autoCapitalize="sentences"
          />
        </View>
      ),
    },
    {
      id: 4,
      details: (
        <View>
          <QuestionBox
            question={'OBSERVASIONES'}
            value={retouchInfo.observation}
            onChangeText={text =>
              setRetouchInfo({
                ...retouchInfo,
                observation: text,
              })
            }
            autoCapitalize="sentences"
          />
          <QuestionBox
            question={'ANTERIOR MICRO'}
            value={retouchInfo.previous_micro}
            onChangeText={text =>
              setRetouchInfo({
                ...retouchInfo,
                previous_micro: text,
              })
            }
            autoCapitalize="sentences"
          />
        </View>
      ),
    },
    {
      id: 5,
      title: 'HISTORIAL MEDICO',
      details: (
        <View>
          <QuestionBox
            question={'PRUEBA DE ALERGIA AL PIGMENTO REALIZADA EL Y LUGAR'}
            value={medicalInfo.que1}
            onChangeText={text =>
              setMedicalInfo({
                ...medicalInfo,
                que1: text,
              })
            }
            autoCapitalize="sentences"
          />
          <QuestionBox
            question={'¿ALERGICA A ALGÚN PRODUCTO COSMÉTICO?'}
            value={medicalInfo.que2}
            onChangeText={text =>
              setMedicalInfo({
                ...medicalInfo,
                que2: text,
              })
            }
            autoCapitalize="sentences"
          />
          <QuestionBox
            question={'¿TOMA ALGÚN MEDICAMENTO?'}
            value={medicalInfo.que3}
            onChangeText={text =>
              setMedicalInfo({
                ...medicalInfo,
                que3: text,
              })
            }
            autoCapitalize="sentences"
          />
          <QuestionBox
            question={
              '¿ESTÁ EN EL MOMENTO DE LA MICROPIGMENTACIÓN CON LA MENSTRUACION? (MAYOR SENSIBILIDAD)'
            }
            value={medicalInfo.que4}
            onChangeText={text =>
              setMedicalInfo({
                ...medicalInfo,
                que4: text,
              })
            }
            autoCapitalize="sentences"
          />
          <QuestionBox
            question={'¿ES DIEBETICA? (ALTERA LA CICATRISACION)'}
            value={medicalInfo.que5}
            onChangeText={text =>
              setMedicalInfo({
                ...medicalInfo,
                que5: text,
              })
            }
            autoCapitalize="sentences"
          />
          <QuestionBox
            question={'¿PADECE HEPATITIS O SIDA?'}
            value={medicalInfo.que6}
            onChangeText={text =>
              setMedicalInfo({
                ...medicalInfo,
                que6: text,
              })
            }
            autoCapitalize="sentences"
          />
          <QuestionBox
            question={
              '¿TOMCA ANTIBIOTICOS? (OJO PUES PROVOCAN DEBILIDAD INMUNOLOGICA, SI TOMA CORTISONA NO SELE HARA)'
            }
            value={medicalInfo.que7}
            onChangeText={text =>
              setMedicalInfo({
                ...medicalInfo,
                que7: text,
              })
            }
            autoCapitalize="sentences"
          />
          <QuestionBox
            question={
              '¿PADESE CARDIOPATIAS? (SANGRARA MAS, EVITAR TOMAR ESE DIA ANTICOAGULANTES)'
            }
            value={medicalInfo.que8}
            onChangeText={text =>
              setMedicalInfo({
                ...medicalInfo,
                que8: text,
              })
            }
            autoCapitalize="sentences"
          />
          <QuestionBox
            question={
              '¿ES PROPENSA A LOS HERPES EN LA ZONA A MICROPEGMENTAR? (TRATARSE UNA SEMANA ANTES PARA PODER TRABAJAR EN UNA PIEL SANA PUES EL VIRUS ESTA EN LA SANGRE Y POSIBILITA LA INFECCION PERDIENDOSE ASI EL PIGMENTO Y EL DISEÑO)'
            }
            value={medicalInfo.que9}
            onChangeText={text =>
              setMedicalInfo({
                ...medicalInfo,
                que9: text,
              })
            }
            autoCapitalize="sentences"
          />
          <QuestionBox
            question={
              '¿ESTÁ RECIBIENDO TRATAMIENTO PSIQUIATRICO O PSICOLOGICO?'
            }
            value={medicalInfo.que10}
            onChangeText={text =>
              setMedicalInfo({
                ...medicalInfo,
                que10: text,
              })
            }
            autoCapitalize="sentences"
          />
          <QuestionBox
            question={
              '¿TIENE CICATRICES EN LA ZONA A TRATAR? (ESPERAR MINIMO UN AÑO PARA QUE REGENERE LA PIEL)'
            }
            value={medicalInfo.que11}
            onChangeText={text =>
              setMedicalInfo({
                ...medicalInfo,
                que11: text,
              })
            }
            autoCapitalize="sentences"
          />
        </View>
      ),
    },
    {
      id: 6,
      title: 'CONTRAINDICACIONES TEMPRORALES',
      details: (
        <View>
          <QuestionBox
            question={'¿ESTÁ EMBARAZADA? (NO SE HARA)'}
            value={medicalInfo.que12}
            onChangeText={text =>
              setMedicalInfo({
                ...medicalInfo,
                que12: text,
              })
            }
            autoCapitalize="sentences"
          />
          <QuestionBox
            question={
              '¿TIENE INFILTRACIONES DE COLÁGENO, SILICONA ETC, ¿EN LOS LABIOS? (ESPERAR 1MES SI ES FIJO, SI ES REABSORVIBLE ESPERAR 6 MESES)'
            }
            value={medicalInfo.que13}
            onChangeText={text =>
              setMedicalInfo({
                ...medicalInfo,
                que13: text,
              })
            }
            autoCapitalize="sentences"
          />
          <QuestionBox
            question={
              '¿TOMA QUIMIO O RADIOTERAPIA? (ESPERAR A FINALIZAR LAS SESIONES)'
            }
            value={medicalInfo.que14}
            onChangeText={text =>
              setMedicalInfo({
                ...medicalInfo,
                que14: text,
              })
            }
            autoCapitalize="sentences"
          />
          <QuestionBox
            question={
              'OPERACIONES DE CIRUGIA ESTETICA FACIAL ESPERAR DE 6 A 12 MESES PARA MICROPEGMENTAR.'
            }
            value={medicalInfo.que15}
            onChangeText={text =>
              setMedicalInfo({
                ...medicalInfo,
                que15: text,
              })
            }
            autoCapitalize="sentences"
          />
        </View>
      ),
    },
    {
      id: 7,
      details: (
        <View>
          <Text
            style={{
              ...styles.subTitle,
              textAlign: 'center',
              marginBottom: wp(2),
            }}>
            CONSENTIMIENTO INFORMADO DE UN TRATAMIENTO DE MICROPIGMENTACIÓN O
            MICROBLADING
          </Text>
          <View style={styles.detailContainer}>
            <View style={styles.rowContainer}>
              <Text style={styles.details}>YO, DÑA</Text>
              <TextInput
                style={styles.formInput}
                value={consentInfo.name}
                onChangeText={text =>
                  setConsetInfo({
                    ...consentInfo,
                    name: text,
                  })
                }
                autoCapitalize="sentences"
              />
            </View>
            <View style={styles.rowContainer}>
              <Text style={styles.details}>CON DNI N.o </Text>
              <TextInput
                style={styles.formInput}
                value={consentInfo.id}
                onChangeText={text =>
                  setConsetInfo({
                    ...consentInfo,
                    id: text,
                  })
                }
                autoCapitalize="sentences"
              />
            </View>
            <View style={styles.rowContainer}>
              <Text style={styles.details}>Y FECHA DE NACIMIENTO</Text>
              <TouchableOpacity
                onPress={() => setOpen({...open, consetDob: !open.consetDob})}
                activeOpacity={0.7}
                style={styles.formInput}>
                <Text>{consentInfo.dob}</Text>
              </TouchableOpacity>
            </View>
            <Text style={styles.details}>
              AUTHORIZO AL TÉCNICO EN MICROPIGMENTACION REALIZARME UN
              TRATAMIENTO DE MICROPIGMENTACION EN:
            </Text>
            <View style={styles.rowContainer}>
              <Text style={styles.details}>ZONDA DE</Text>
              <TextInput
                style={styles.formInput}
                value={consentInfo.zone1}
                onChangeText={text =>
                  setConsetInfo({
                    ...consentInfo,
                    zone1: text,
                  })
                }
                autoCapitalize="sentences"
              />
              <Text style={styles.details}>FECHA</Text>
              <TouchableOpacity
                onPress={() =>
                  setOpen({...open, consetDate1: !open.consetDate1})
                }
                activeOpacity={0.7}
                style={styles.formInput}>
                <Text>{consentInfo.date1}</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.rowContainer}>
              <Text style={styles.details}>ZONDA DE</Text>
              <TextInput
                style={styles.formInput}
                value={consentInfo.zone2}
                onChangeText={text =>
                  setConsetInfo({
                    ...consentInfo,
                    zone2: text,
                  })
                }
                autoCapitalize="sentences"
              />
              <Text style={styles.details}>FECHA</Text>
              <TouchableOpacity
                onPress={() =>
                  setOpen({...open, consetDate2: !open.consetDate2})
                }
                activeOpacity={0.7}
                style={styles.formInput}>
                <Text>{consentInfo.date2}</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.rowContainer}>
              <Text style={styles.details}>ZONDA DE</Text>
              <TextInput
                style={styles.formInput}
                value={consentInfo.zone3}
                onChangeText={text =>
                  setConsetInfo({
                    ...consentInfo,
                    zone3: text,
                  })
                }
                autoCapitalize="sentences"
              />
              <Text style={styles.details}>FECHA</Text>
              <TouchableOpacity
                onPress={() =>
                  setOpen({...open, consetDate3: !open.consetDate3})
                }
                activeOpacity={0.7}
                style={styles.formInput}>
                <Text>{consentInfo.date3}</Text>
              </TouchableOpacity>
            </View>
            <Text style={styles.details}>
              RECONOZCO QUE SE ME HA INFORMADO DE ESTE TRATAMIENTO Y SE ME HAN
              RESPONDIDO TODAS LAS PREGUNTAS QUE ME HAN SURGIDO SOBRE ESTE
              TRATAMIENTO DE MICROPIGMENTACION. ASIMISMO, HE VISTO LA
              APARATOLOGÍA Y LOS PIGMENTOS CON LOS QUE SE ME VA A REALIZAR DICHO
              SERVICIO Y ESTOY CONFORME TRAS HABERME MOSTRADO QUE ESTAN
              AUTORIZADOS POR EL MINISTERIO DE SANIDAD. SE ME INFORMA QUE TODOS
              LOS SUPLIDOS PARA LA REALIZACIÓN DE LA MICROPIGMENTACIÓN QUE SE
              VAN A UTILIZAR SON DESECHABLES, LOS CUALES SE ABREN Y DESECHAN EN
              MI PRESENCIA. EL TÉCNICO ESPECIALISTA ME HA REALIZADO UN VISAGISMO
              (DISEÑO) PREVIO AL TRATAMIENTO, ESTANDO CONFORME CON EL MISMO,
              TANTO EN EL COLOR COMO EN LA FORMA. EXIMO DE TODA RESPONSABILIDAD
              A LA TÉCNICA EN MICROPIGMENTACIÓN AL CENTRO DE BELLEZA{' '}
            </Text>
            <TextInput
              style={styles.formInput}
              value={consentInfo.center_name}
              onChangeText={text =>
                setConsetInfo({
                  ...consentInfo,
                  center_name: text,
                })
              }
              autoCapitalize="sentences"
            />
            <Text style={styles.details}>
              EN CASO DE SURGIR ALGUN PROBLEMA POR NO HABERLE INFORMADO SOBRE
              ALGUN ASPECTO DEL HISTORIAL CLÍNICO.
            </Text>
            <Text style={styles.details}>
              AUTORIZO LA REALIZACIÓN DE FOTOGRAFIAS DEL ANTES Y EL DESPUÉS DE
              LA MICROPIGMENTACION REALIZADA. LA CUSTODIA Y GESTIÓN DE DIC HAS
              FOTOGRAFIAS ESTAN SUJETAS A LA LEY DE PROTECCION DE DATOS DE
              CARÁCTER PERSONAL.
            </Text>
            <Text style={styles.details}>
              AUTORIZO A DAR USO PROFESIONAL AL FOTOGRAFIADO QUE SE ME HACE DEL
              ANTES Y EL DESPUES DONDE SE REQUIERA, COMO POR EJEMPLO EN P AGINA
              WEB EN INTERNET.
            </Text>
            <Text style={styles.details}>
              {'\n'}
              SE ME INFORMA QUE LA MICRPIGMENTACION ES UNA TECNICA DE
              MICROPLANTACION DE PIGMENTOS EN LA PIEL, CUYOS RESULTADOS SE IRÁN
              DESVANECIENDO GRADUALMENTE POR CAUSAS DIVERSAS COMO EL TONO
              APLICADO, COSMETICA O CUIDADOS INAPROPRIADOS Y OTRAS
              PARTICULARIDADES, EN CONSECUENSIA, ACEPTO EL TRATAMIENTO DE
              MICROPIGMENTACION VOLUNTARIAMENTE. RECONOZCO QUE ESTE TRATAMIENTO
              CONLLEVA UN PROCESO DE VARIOS RETOQUES Y QUE HASTA LA FINALIZACION
              DE MISMO NO SE PUEDE REALIZAR UNA VALORACIÓN DEL TRABAJO, ES POR
              ESTE MOTIVO QUE ACEPTO QUE NO DEVUELVE EL DINERO SI LLEGADO AL
              CASO, POR MOTIVOS INFUNDADAS NO ESTUVIERA DE ACUERDO CON EL
              TRABAJO SIN HABER DEJADO EL TIEMPO OPORTUNO DE FINALIZACIÓN DEL
              TRATAMIENTO.
            </Text>
            <Text style={styles.details}>
              {'\n'}
              DECLARO QUE SE ME HA DADO UNA HOJA INFORMATIVA ANTES DE REALIZARME
              LA MICROPIGMENTACIÓN, DE LOS CUIDADOS ANTERIORES COMO MEDI DA
              PRVENTIVA EN LO QUE TIENE QUE VER, CON LA APARICION DE HERPES Y
              ASIMISMO SE ME HACE ENTREGA DE LOS CUIDADOS POSTERIORES PARA LA
              PRONTA REGENRACIÓN DE LA PIEL Y ASUMO S EGUIR LOS PASOS QUE SE ME
              SEÑALAN POR LO QUE ASUMO COMO MÍOS LOS PROBLEMAS DERIVADOS DE UN
              CUIDADO POST TRATAMIENTO DE MICROPIGMENTACIÓN NO ADECUADO COMO
              CONSECUENCIA DE LAS PARTICULARIDADES DE MI CASO, EXIMIENDO A LA
              TÉCNIO LILIT AVOYAN DE LOS INCONVENIENTES QUE PUEDAN SURGIR ASI
              COMO LOS PROBLEMAS QUE PUEDAN SURGIR POR NO HABERLE INFORMADO
              SOBRE ALGUN ASPECTO DEL HISTORIAL CLÍNICO.
            </Text>
            <Text style={styles.details}>
              ESTOY CONFORME CON EL TRATAMIENTO Y EL PRECIO GLOBAL
              PRESUPUESTADO, QUE INCLUYE DE SENSIBILIDAD ALÉRGICA. DE{' '}
            </Text>
            <View style={styles.rowContainer}>
              <TextInput
                style={styles.formInput}
                value={consentInfo.from}
                onChangeText={text =>
                  setConsetInfo({
                    ...consentInfo,
                    from: text,
                  })
                }
                autoCapitalize="sentences"
              />
              <Text>€</Text>
            </View>
            <Text style={styles.details}>
              {'\n'}Y PARA QUE CONSTE QUE ESTOY CONFORME Y EN PLENAS FACULTADES
              CON TODO LO ANTERIORMENTE EXPUESTO FIRMO LA HOJA DE CONSENTIMIENTO
              Y RECONOCIMIENTO EN LA FECHA INDICADA.{'\n'}
            </Text>
            <View style={styles.rowContainer}>
              <Text style={styles.details}>FIRMA Y CONFORME CLIENTE:</Text>
              <TouchableOpacity
                style={styles.underlinedBox}
                activeOpacity={0.7}
                onPress={() => setModalVisible(!modalVisible)}>
                {signature?.length > 0 && (
                  <Image
                    source={{uri: signature}}
                    style={styles.displaySignature}
                  />
                )}
              </TouchableOpacity>
            </View>
            <View style={styles.rowContainer}>
              <Text style={styles.details}>FECHA: </Text>
              <TouchableOpacity
                onPress={() =>
                  setOpen({...open, consentDate4: !open.consentDate4})
                }
                activeOpacity={0.7}
                style={styles.formInput}>
                <Text>{consentInfo.signdate}</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      ),
    },
  ];

  const saveForm = async () => {
    if (!personalInfo.name) {
      alert('El nombre de usuario no debe estar vacio');
      return;
    }
    if (!personalInfo.id) {
      alert('El id de usuario no debe estar vacio');
      return;
    }
    if(!signature){
      alert('Debe firmar para poder guardar la información');
      return;
    }
    setLoading(true);
    const data = {
      personalInfo,
      treatmentInfo,
      retouchInfo,
      medicalInfo,
      consentInfo,
      signature,
    };
    const file = await saveDataToFile(data);
    console.log(file);
    if (userData) {
      await updateRecord(userData.id, data)
        .then(result => {
          alert(result);
        })
        .catch(error => console.log(error))
        .finally(() => {
          setLoading(false);
          props.navigation.navigate('Dashboard');
        });
    } else {
      await saveData(data)
        .then(result => {
          alert(result);
        })
        .catch(error => console.log(error))
        .finally(() => {
          setLoading(false);
          props.navigation.navigate('Dashboard');
        });
    }
  };

  return (
    <View style={styles.container}>
      <DatePicker
        modal
        open={open.dob}
        date={new Date()}
        onConfirm={date => {
          setOpen({...open, dob: !open.dob});
          setPersonalInfo({
            ...personalInfo,
            dob: moment(date).format('DD/MM/YYYY'),
          });
        }}
        onCancel={() => {
          setOpen({...open, dob: !open.dob});
        }}
        mode="date"
      />
      <DatePicker
        modal
        open={open.date1}
        date={new Date()}
        onConfirm={date => {
          setOpen({...open, date1: !open.date1});
          setPersonalInfo({
            ...personalInfo,
            date1: moment(date).format('DD/MM/YYYY'),
          });
        }}
        onCancel={() => {
          setOpen({...open, date1: !open.date1});
        }}
        mode="date"
      />
      <DatePicker
        modal
        open={open.date2}
        date={new Date()}
        onConfirm={date => {
          setOpen({...open, date2: !open.date2});
          setPersonalInfo({
            ...personalInfo,
            date2: moment(date).format('DD/MM/YYYY'),
          });
        }}
        onCancel={() => {
          setOpen({...open, date2: !open.date2});
        }}
        mode="date"
      />
      <DatePicker
        modal
        open={open.treatmentDate}
        date={new Date()}
        onConfirm={date => {
          setOpen({...open, treatmentDate: !open.treatmentDate});
          setTreatmentInfo({
            ...treatmentInfo,
            date: moment(date).format('DD/MM/YYYY'),
          });
        }}
        onCancel={() => {
          setOpen({...open, treatmentDate: !open.treatmentDate});
        }}
        mode="date"
      />
      <DatePicker
        modal
        open={open.retouchDate1}
        date={new Date()}
        onConfirm={date => {
          setOpen({...open, retouchDate1: !open.retouchDate1});
          setRetouchInfo({
            ...retouchInfo,
            retouch1: {
              ...retouchInfo.retouch1,
              date: moment(date).format('DD/MM/YYYY'),
            },
          });
        }}
        onCancel={() => {
          setOpen({...open, retouchDate1: !open.retouchDate1});
        }}
        mode="date"
      />
      <DatePicker
        modal
        open={open.retouchDate2}
        date={new Date()}
        onConfirm={date => {
          setOpen({...open, retouchDate2: !open.retouchDate2});
          setRetouchInfo({
            ...retouchInfo,
            retouch2: {
              ...retouchInfo.retouch2,
              date: moment(date).format('DD/MM/YYYY'),
            },
          });
        }}
        onCancel={() => {
          setOpen({...open, retouchDate2: !open.retouchDate2});
        }}
        mode="date"
      />
      <DatePicker
        modal
        open={open.consetDob}
        date={new Date()}
        onConfirm={date => {
          setOpen({...open, consetDob: !open.consetDob});
          setConsetInfo({
            ...consentInfo,
            dob: moment(date).format('DD/MM/YYYY'),
          });
        }}
        onCancel={() => {
          setOpen({...open, consetDob: !open.consetDob});
        }}
        mode="date"
      />
      <DatePicker
        modal
        open={open.consetDate1}
        date={new Date()}
        onConfirm={date => {
          setOpen({...open, consetDate1: !open.consetDate1});
          setConsetInfo({
            ...consentInfo,
            date1: moment(date).format('DD/MM/YYYY'),
          });
        }}
        onCancel={() => {
          setOpen({...open, consetDate1: !open.consetDate1});
        }}
        mode="date"
      />

      <DatePicker
        modal
        open={open.consetDate2}
        date={new Date()}
        onConfirm={date => {
          setOpen({...open, consetDate2: !open.consetDate2});
          setConsetInfo({
            ...consentInfo,
            date2: moment(date).format('DD/MM/YYYY'),
          });
        }}
        onCancel={() => {
          setOpen({...open, consetDate2: !open.consetDate2});
        }}
        mode="date"
      />
      <DatePicker
        modal
        open={open.consetDate3}
        date={new Date()}
        onConfirm={date => {
          setOpen({...open, consetDate3: !open.consetDate3});
          setConsetInfo({
            ...consentInfo,
            date3: moment(date).format('DD/MM/YYYY'),
          });
        }}
        onCancel={() => {
          setOpen({...open, consetDate3: !open.consetDate3});
        }}
        mode="date"
      />
      <DatePicker
        modal
        open={open.consentDate4}
        date={new Date()}
        onConfirm={date => {
          setOpen({...open, consentDate4: !open.consentDate4});
          setConsetInfo({
            ...consentInfo,
            signdate: moment(date).format('DD/MM/YYYY'),
          });
        }}
        onCancel={() => {
          setOpen({...open, consentDate4: !open.consentDate4});
        }}
        mode="date"
      />
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          setModalVisible(!modalVisible);
        }}>
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <View
              style={{
                ...styles.buttonContainer,
                justifyContent: 'space-between',
              }}>
              <Text style={styles.subTitle}>DRAW YOUR SIGNATURE</Text>
              <TouchableOpacity
                activeOpacity={0.7}
                onPress={() => {
                  setModalVisible(!modalVisible);
                }}>
                <Icon name={'closecircleo'} size={25} color={Colors.greyText} />
              </TouchableOpacity>
            </View>
            <SignatureCapture
              style={[{flex: 1}, styles.signature]}
              ref={sign}
              onSaveEvent={_onSaveEvent}
              saveImageFileInExtStorage={true}
              showNativeButtons={false}
              showTitleLabel={false}
              backgroundColor={Colors.white}
              strokeColor={Colors.black}
              minStrokeWidth={4}
              maxStrokeWidth={4}
              viewMode={'portrait'}
            />
            <View
              style={{
                ...styles.buttonContainer,
                justifyContent: 'space-between',
              }}>
              <ButtonBox
                title="Save"
                onPress={() => {
                  saveSign();
                }}
              />
              <ButtonBox
                title="Clear"
                onPress={() => {
                  resetSign();
                }}
              />
            </View>
          </View>
        </View>
      </Modal>
      <SafeAreaView style={{flex: 1}}>
        <View style={styles.buttonContainer}>
          {currentForm > 0 ? (
            <ButtonBox
              title="Previo"
              style={styles.buttonStyle}
              titleStyle={styles.btnTitle}
              onPress={() => setForm(c => c - 1)}
            />
          ) : (
            <ButtonBox
              title="Atrás"
              style={styles.buttonStyle}
              titleStyle={styles.btnTitle}
              onPress={() => props.navigation.goBack()}
            />
          )}
          <Text style={styles.titleStyle}>{fieldData[currentForm]?.title}</Text>
          {currentForm < fieldData.length - 1 && (
            <ButtonBox
              title="Siguente"
              style={styles.buttonStyle}
              titleStyle={styles.btnTitle}
              onPress={() => setForm(currentForm => currentForm + 1)}
            />
          )}
          {currentForm == fieldData.length - 1 && (
            <ButtonBox
              title="Guardar"
              style={styles.buttonStyle}
              titleStyle={styles.btnTitle}
              isLoading={loading}
              onPress={saveForm}
            />
          )}
        </View>
        <KeyboardAvoidingView
          style={{flex: 1}}
          behavior={Platform.OS === 'ios' ? 'padding' : null}>
          <View style={styles.cardContainer}>
            <ScrollView
              style={styles.scrollContainer}
              showsVerticalScrollIndicator={false}>
              {fieldData[currentForm]?.details}
              <View style={{paddingBottom: hp(8)}} />
            </ScrollView>
          </View>
        </KeyboardAvoidingView>
      </SafeAreaView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.themePurple,
  },
  buttonContainer: {
    width: '94%',
    flexDirection: 'row',
    marginHorizontal: wp(2),
    marginVertical: hp(2),
  },
  titleStyle: {
    flex: 3,
    color: Colors.white,
    fontFamily: 'Poppins-Medium',
    fontSize: 16,
    textAlign: 'center',
    alignSelf: 'center',
  },
  subTitle: {
    fontFamily: 'Poppins-Medium',
    fontSize: 16,
    color: Colors.themeGrey,
  },
  buttonStyle: {
    backgroundColor: Colors.white,
    paddingHorizontal: 5,
    width: wp(22),
    borderRadius: 4,
    paddingVertical: hp(1),
    alignSelf: 'center',
  },
  btnTitle: {
    color: Colors.greyText,
    fontSize: 14,
  },
  cardContainer: {
    flex: 1,
    backgroundColor: Colors.white,
    borderTopEndRadius: hp(3),
    borderTopStartRadius: hp(3),
    padding: hp(1.5),
  },
  inputBox: {
    marginVertical: hp(1.2),
  },
  scrollContainer: {
    flex: 1,
    paddingBottom: hp(3),
  },
  detailContainer: {
    flex: 1,
    marginHorizontal: wp(1),
  },
  rowContainer: {
    flexDirection: 'row',
    alignItems: 'flex-end',
  },
  formInput: {
    fontFamily: 'Poppins-Regular',
    fontSize: 14,
    color: Colors.black,
    borderBottomColor: Colors.greyText,
    borderBottomWidth: 1,
    flex: 1,
    marginHorizontal: wp(1),
  },
  underlinedBox: {
    borderBottomColor: Colors.greyText,
    borderBottomWidth: 1,
    flex: 1,
    height: hp(3),
    paddingBottom: 70,
  },
  details: {
    fontFamily: 'Poppins-Regular',
    fontSize: 14,
    color: Colors.black,
    textAlign: 'justify',
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.white + '8C',
  },
  modalView: {
    flex: 1,
    maxHeight: '92%',
    width: '90%',
    margin: 20,
    backgroundColor: Colors.white,
    borderRadius: 5,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
    padding: wp(2),
  },
  signature: {
    flex: 1,
    width: '90%',
  },
  displaySignature: {
    width: 140,
    height: 70,
    resizeMode: 'cover',
  },
  dateInputBox: {
    borderWidth: 1,
    borderRadius: 4,
    borderColor: Colors.themeGrey,
    marginVertical: hp(1.2),
  },
  dateText: {
    padding: hp(1.5),
    fontFamily: 'Poppins-Medium',
    fontSize: 16,
    color: Colors.themeGrey,
  },
});

export default AddRecordScreen;
