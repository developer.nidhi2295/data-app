import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  View,
  SafeAreaView,
  ActivityIndicator,
  FlatList,
  TouchableOpacity,
  Alert,
  Text,
} from 'react-native';
import SearchBox from '../components/SearchBox';
import ButtonBox from '../components/ButtonBox';
import UserCard from '../components/UserCard';
import Colors from '../constants/Colors';
import {
  checkForTable,
  getAllData,
  deleteRecord,
} from '../database/user-operations';
import Icon from 'react-native-vector-icons/AntDesign';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from '../utils/responsive-screen';

const Dashboard = ({navigation}) => {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(false);
  const [search, setSearchQuery] = useState('');
  const [searchData, setSearchData] = useState();

  const getUserData = () => {
    checkForTable().then(async res => {
      console.log('****Dashboard Getting Data');
      await getAllData()
        .then(result => {
          setData(result);
        })
        .catch(error => console.log(error));
      console.log('****Dashboard Done Getting Data');
    });
  };

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      setLoading(true);
      getUserData();
      setLoading(false);
    });
    return unsubscribe;
  }, [navigation]);

  const deleteUserRecord = id => {
    Alert.alert('Delete', 'Do you want to delete Record?', [
      {
        text: 'Cancel',
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel',
      },
      {
        text: 'OK',
        onPress: () =>
          deleteRecord(id).then(msg => {
            alert(msg);
            getUserData();
          }),
      },
    ]);
  };

  const renderData = ({item, index}) => {
    return (
      <UserCard
        name={item.name}
        id={item.user_id}
        onDelete={() => deleteUserRecord(item.id)}
        onEdit={() => navigation.navigate('AddRecord', {userData: item})}
      />
    );
  };

  const onSearch = searchQuery => {
    setSearchQuery(searchQuery);
    if (searchQuery.length === 0) {
      setSearchData(null);
    } else {
      const search = data.filter(user =>
        user.phone.toString().includes(searchQuery),
      );
      setSearchData(search);
    }
  };

  return (
    <View style={{flex: 1}}>
      <View style={styles.headerStyle}>
        <SearchBox value={search} onChangeText={onSearch} />
      </View>
      <SafeAreaView style={styles.container}>
        {search.length > 0 && !searchData?.length > 0 && (
          <Text style={styles.text}>No Record Found</Text>
        )}
        {loading ? (
          <View style={styles.centerData}>
            <ActivityIndicator size={'small'} color={Colors.themePurple} />
          </View>
        ) : data.length > 0 ? (
          <>
            <FlatList
              data={searchData ? searchData : data}
              renderItem={renderData}
              keyExtractor={item => item.id}
            />
            <TouchableOpacity
              style={styles.addButton}
              activeOpacity={0.6}
              onPress={() => navigation.navigate('AddRecord')}>
              <Icon name="plus" size={hp(3.5)} color={Colors.white} />
            </TouchableOpacity>
          </>
        ) : (
          <View style={styles.centerData}>
            <ButtonBox
              title={'Agregar registro'}
              onPress={() => navigation.navigate('AddRecord')}
            />
          </View>
        )}
      </SafeAreaView>
    </View>
  );
};

const styles = StyleSheet.create({
  headerStyle: {
    backgroundColor: Colors.themePurple,
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  container: {
    flex: 1,
    backgroundColor: Colors.white,
  },
  centerData: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  addButton: {
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    bottom: hp(3),
    right: hp(2),
    padding: wp(3.5),
    backgroundColor: Colors.themePurple,
    borderRadius: 100,
  },
  text: {
    fontFamily: 'Poppins-Regular',
    fontSize: 22,
    color: Colors.black,
    textAlign: 'center',
    marginTop: hp(2)
  },
});

export default Dashboard;
