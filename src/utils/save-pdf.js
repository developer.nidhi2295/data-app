import React from 'react';
import {Platform, PermissionsAndroid} from 'react-native';
import RNHTMLtoPDF from 'react-native-html-to-pdf';

const isPermitted = async () => {
  if (Platform.OS === 'android') {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        {
          title: 'External Storage Write Permission',
          message: 'App needs access to Storage data',
        },
      );
      return granted === PermissionsAndroid.RESULTS.GRANTED;
    } catch (err) {
      console.log('Write permission err', err);
      return false;
    }
  } else {
    return true;
  }
};

export const saveDataToFile = async data => {
  if (await isPermitted()) {
    let options = {
      //Content to print
      html: `<HTML>
      <HEAD>
          <META http-equiv="Content-Type" content="text/html; charset=UTF-8">
          <TITLE>pdf-html</TITLE>
          <META name="generator" content="BCL easyConverter SDK 5.0.252">
          <META name="author" content="Arsen Khachatryan">
          <STYLE type="text/css">
              body {
                  margin-top: 0px;
                  margin-left: 0px;
              }
              #page_1 {
                  position: relative;
                  overflow: hidden;
                  margin: 21px 0px 64px 57px;
                  padding: 0px;
                  border: none;
                  width: 737px;
              }
              #page_2 {
                  position: relative;
                  overflow: hidden;
                  margin: 42px 0px 549px 57px;
                  padding: 0px;
                  border: none;
                  width: 737px;
              }
              .ft0 {
                  font: bold 15px 'Calibri';
                  line-height: 18px;
              }
              .ft1 {
                  font: 15px 'Calibri';
                  line-height: 18px;
              }
              .ft2 {
                  font: 9px 'Calibri';
                  line-height: 11px;
                  position: relative;
                  bottom: 5px;
              }
      
              .ft3 {
                  font: 15px 'Times New Roman';
                  line-height: 17px;
              }
      
              .ft4 {
                  font: 15px 'Calibri';
                  margin-left: 17px;
                  line-height: 18px;
              }
              .ft5 {
                  font: 15px 'Calibri';
                  margin-left: 17px;
                  line-height: 20px;
              }
              .ft6 {
                  font: 15px 'Calibri';
                  line-height: 20px;
              }
              .ft7 {
                  font: 15px 'Calibri';
                  margin-left: 17px;
                  line-height: 21px;
              }
      
              .ft8 {
                  font: 15px 'Calibri';
                  line-height: 21px;
              }
              .ft9 {
                  font: bold 15px 'Calibri';
                  text-decoration: underline;
                  line-height: 18px;
              }
              .ft10 {
                  font: 15px 'Calibri';
                  margin-left: 17px;
                  line-height: 19px;
              }
              .ft11 {
                  font: 15px 'Calibri';
                  line-height: 19px;
              }
              .ft12 {
                  font: 8px 'Calibri';
                  line-height: 10px;
              }
              .ft13 {
                  font: 7px 'Calibri';
                  line-height: 9px;
              }
              .p0 {
                  text-align: left;
                  margin-top: 0px;
                  margin-bottom: 0px;
              }
              .p1 {
                  text-align: left;
                  margin-top: 37px;
                  margin-bottom: 0px;
              }
              .p2 {
                  text-align: left;
                  margin-top: 16px;
                  margin-bottom: 0px;
              }
              .p3 {
                  text-align: left;
                  margin-top: 14px;
                  margin-bottom: 0px;
              }
              .p4 {
                  text-align: left;
                  margin-top: 17px;
                  margin-bottom: 0px;
              }
              .p5 {
                  text-align: left;
                  margin-top: 23px;
                  margin-bottom: 0px;
              }
      
              .p6 {
                  text-align: left;
                  padding-left: 286px;
                  margin-top: 16px;
                  margin-bottom: 0px;
              }
      
              .p7 {
                  text-align: left;
                  padding-left: 24px;
                  margin-top: 13px;
                  margin-bottom: 0px;
              }
      
              .p8 {
                  text-align: left;
                  padding-left: 24px;
                  margin-top: 3px;
                  margin-bottom: 0px;
              }
      
              .p9 {
                  text-align: left;
                  padding-left: 24px;
                  margin-top: 4px;
                  margin-bottom: 0px;
              }
      
              .p10 {
                  text-align: left;
                  padding-left: 48px;
                  padding-right: 61px;
                  margin-top: 4px;
                  margin-bottom: 0px;
                  text-indent: -24px;
              }
      
              .p11 {
                  text-align: left;
                  padding-left: 24px;
                  margin-top: 2px;
                  margin-bottom: 0px;
              }
      
              .p12 {
                  text-align: left;
                  padding-left: 48px;
                  padding-right: 64px;
                  margin-top: 3px;
                  margin-bottom: 0px;
                  text-indent: -24px;
              }
      
              .p13 {
                  text-align: left;
                  padding-left: 24px;
                  margin-top: 0px;
                  margin-bottom: 0px;
              }
      
              .p14 {
                  text-align: left;
                  padding-left: 252px;
                  margin-top: 7px;
                  margin-bottom: 0px;
              }
      
              .p15 {
                  text-align: left;
                  padding-left: 48px;
                  padding-right: 67px;
                  margin-top: 3px;
                  margin-bottom: 0px;
                  text-indent: -24px;
              }
      
              .p16 {
                  text-align: left;
                  padding-left: 125px;
                  margin-top: 0px;
                  margin-bottom: 0px;
              }
      
              .p17 {
                  text-align: left;
                  padding-left: 314px;
                  margin-top: 2px;
                  margin-bottom: 0px;
              }
      
              .p18 {
                  text-align: left;
                  margin-top: 12px;
                  margin-bottom: 0px;
              }
      
              .p19 {
                  text-align: left;
                  margin-top: 13px;
                  margin-bottom: 0px;
              }
      
              .p20 {
                  text-align: justify;
                  padding-right: 52px;
                  margin-top: 1px;
                  margin-bottom: 0px;
              }
      
              .p21 {
                  text-align: left;
                  margin-top: 4px;
                  margin-bottom: 0px;
              }
      
              .p22 {
                  text-align: left;
                  margin-top: 1px;
                  margin-bottom: 0px;
              }
      
              .p23 {
                  text-align: left;
                  padding-right: 55px;
                  margin-top: 1px;
                  margin-bottom: 0px;
              }
      
              .p24 {
                  text-align: left;
                  margin-top: 3px;
                  margin-bottom: 0px;
              }
      
              .p25 {
                  text-align: left;
                  padding-right: 49px;
                  margin-top: 12px;
                  margin-bottom: 0px;
              }
      
              .p26 {
                  text-align: left;
                  padding-right: 56px;
                  margin-top: 3px;
                  margin-bottom: 0px;
              }
      
              .p27 {
                  text-align: left;
                  padding-right: 53px;
                  margin-top: 15px;
                  margin-bottom: 0px;
              }
      
              .p28 {
                  text-align: left;
                  margin-top: 27px;
                  margin-bottom: 0px;
              }
      
              .p29 {
                  text-align: left;
                  margin-top: 0px;
                  margin-bottom: 0px;
                  white-space: nowrap;
              }
      
              .td0 {
                  padding: 0px;
                  margin: 0px;
                  width: 288px;
                  vertical-align: bottom;
              }
      
              .td1 {
                  padding: 0px;
                  margin: 0px;
                  width: 183px;
                  vertical-align: bottom;
              }
      
              .tr0 {
                  height: 10px;
              }
      
              .t0 {
                  width: 471px;
                  margin-top: 12px;
                  font: 7px 'Calibri';
              }
          </STYLE>
      </HEAD>
      
      <BODY>
          <DIV id="page_1">
              <P class="p0 ft0">DATOS CLIENTE</P>
              <P class="p1 ft1">Nombre: <u>${data.personalInfo.name}</u> Teléfonos: <u>${data.personalInfo.phone}</u>____</P>
              <P class="p2 ft1">Dirección: <u>${data.personalInfo.address}</u></P>
              <P class="p2 ft1">DNI: <u>${data.personalInfo.id}</u> Fecha de Nacimiento:<u>${data.personalInfo.dob}</u></P>
              <P class="p2 ft1">Precio del Tratamiento: <u>${data.personalInfo.treatment_price}</u> Precio del Repaso: <u>${data.personalInfo.review_price}</u></P>
              <P class="p3 ft1">Fecha 1<SPAN class="ft2">a </SPAN>Visita: <u>${data.personalInfo.date1}</u> 2<SPAN class="ft2">a
                  </SPAN>Visita: <u>${data.personalInfo.date2}</u> Prueba Alérgia: <u>${data.personalInfo.allergy_test}</u></P>
              <P class="p4 ft0">TRATAMIENTOS REALIZADOS</P>
              <P class="p1 ft1">Fecha: <u>${data.treatmentInfo.date}</u> Zona <u>${data.treatmentInfo.zone}</u> Técnica <u>${data.treatmentInfo.technique}</u> Color <u>${data.treatmentInfo.color}</u>
                  Aguja <u>${data.treatmentInfo.needle}</u></P>
              <P class="p5 ft0">RETOQUE:</P>
              <P class="p2 ft1">Fecha: <u>${data.retouchInfo.retouch1.date}</u> Zona <u>${data.retouchInfo.retouch1.zone}</u> Técnica <u>${data.retouchInfo.retouch1.technique}</u> Color <u>${data.retouchInfo.retouch1.color}</u>
                  Aguja <u>${data.retouchInfo.retouch1.needle}</u> </P>
              <P class="p5 ft0">RETOQUE:</P>
              <P class="p2 ft1">Fecha: <u>${data.retouchInfo.retouch2.date}</u> Zona <u>${data.retouchInfo.retouch2.zone}</u> Técnica <u>${data.retouchInfo.retouch2.technique}</u> Color <u>${data.retouchInfo.retouch2.color}</u>
                  Aguja <u>${data.retouchInfo.retouch2.needle}</u></P>
              <P class="p1 ft0">RETOQUE: <u>${data.retouchInfo.retouchDescription}</u></P>
              <P class="p2 ft0">OBSERVASIONES: <u>${data.retouchInfo.observation}</u></P>
              <P class="p2 ft0">ANTERIOR MICRO: <u>${data.retouchInfo.previous_micro}</u></P>
              <P class="p6 ft0">HISTORIAL MEDICO</P>
              <P class="p7 ft1"><SPAN class="ft3">•</SPAN><SPAN class="ft4">PRUEBA DE ALERGIA AL PIGMENTO REALIZADA EL:
              <u>${data.medicalInfo.que1}</u> LUGAR: <u>${data.medicalInfo.que1}</u></SPAN></P>
              <P class="p8 ft1"><SPAN class="ft3">•</SPAN><SPAN class="ft4">¿ALERGICA A ALGÚN PRODUCTO COSMÉTICO?</SPAN> <u>${data.medicalInfo.que2}</u></P>
              <P class="p8 ft1"><SPAN class="ft3">•</SPAN><SPAN class="ft4">¿TOMA ALGÚN MEDICAMENTO?</SPAN> <u>${data.medicalInfo.que3}</u></P>
              <P class="p9 ft1"><SPAN class="ft3">•</SPAN><SPAN class="ft4">¿ESTÁ EN EL MOMENTO DE LA MICROPIGMENTACIÓN CON LA
                      MENSTRUACION? (MAYOR SENSIBILIDAD)</SPAN> <u>${data.medicalInfo.que4}</u></P>
              <P class="p8 ft1"><SPAN class="ft3">•</SPAN><SPAN class="ft4">¿ES DIEBETICA? (ALTERA LA CICATRISACION)</SPAN>
              <u>${data.medicalInfo.que5}</u>
              </P>
              <P class="p8 ft1"><SPAN class="ft3">•</SPAN><SPAN class="ft4">¿PADECE HEPATITIS O SIDA?</SPAN> <u>${data.medicalInfo.que6}</u></P>
              <P class="p10 ft6"><SPAN class="ft3">•</SPAN><SPAN class="ft5">¿TOMCA ANTIBIOTICOS? (OJO PUES PROVOCAN DEBILIDAD
                      INMUNOLOGICA, SI TOMA CORTISONA NO SE LE HARA)</SPAN> <u>${data.medicalInfo.que7}</u></P>
              <P class="p11 ft1"><SPAN class="ft3">•</SPAN><SPAN class="ft4">¿PADESE CARDIOPATIAS? (SANGRARA MAS, EVITAR TOMAR
                      ESE DIA ANTICOAGULANTES)</SPAN> <u>${data.medicalInfo.que8}</u></P>
              <P class="p12 ft8"><SPAN class="ft3">•</SPAN><SPAN class="ft7">¿ES PROPENSA A LOS HERPES EN LA ZONA A
                      MICROPEGMENTAR? (TRATARSE UNA SEMANA ANTES PARA PODER TRABAJAR EN UNA PIEL SANA PUES EL VIRUS ESTA EN LA
                      SANGRE Y POSIBILITA LA INFECCION PERDIENDOSE ASI EL PIGMENTO Y EL DISEÑO)</SPAN> <u>${data.medicalInfo.que9}</u></P>
              <P class="p13 ft1"><SPAN class="ft3">•</SPAN><SPAN class="ft4">¿ESTÁ RECIBIENDO TRATAMIENTO PSIQUIATRICO O
                      PSICOLOGICO?</SPAN> <u>${data.medicalInfo.que10}</u></P>
              <P class="p8 ft1"><SPAN class="ft3">•</SPAN><SPAN class="ft4">¿TIENE CICATRICES EN LA ZONA A TRATAR? (ESPERAR
                      MINIMO UN AÑO PARA QUE REGENERE LA PIEL)</SPAN> <u>${data.medicalInfo.que11}</u></P>
              <P class="p14 ft9">CONTRAINDICACIONES TEMPRORALES</P>
              <P class="p13 ft1"><SPAN class="ft3">•</SPAN><SPAN class="ft4">¿ESTÁ EMBARAZADA? (NO SE HARA)</SPAN> <u>${data.medicalInfo.que12}</u></P>
              <P class="p15 ft11"><SPAN class="ft3">•</SPAN><SPAN class="ft10">¿TIENE INFILTRACIONES DE COLÁGENO, SILICONA
                      ETC, ¿EN LOS LABIOS? (ESPERAR 1MES SI ES FIJO, SI ES REABSORVIBLE ESconsentInfo.PERAR 6 MESES)</SPAN> <u>${data.medicalInfo.que13}</u></P>
              <P class="p8 ft1"><SPAN class="ft3">•</SPAN><SPAN class="ft4">¿TOMA QUIMIO O RADIOTERAPIA? (ESPERAR A FINALIZAR
                      LAS SESIONES)</SPAN> <u>${data.medicalInfo.que14}</u></P>
              <P class="p9 ft1"><SPAN class="ft3">•</SPAN><SPAN class="ft4">OPERACIONES DE CIRUGIA ESTETICA FACIAL ESPERAR DE
                      6 A 12 MESES PARA MICROPEGMENTAR.</SPAN> <u>${data.medicalInfo.que15}</u></P>
          </DIV>
          <DIV id="page_2">
      
      
              <P class="p16 ft1">CONSENTIMIENTO INFORMADO DE UN TRATAMIENTO DE MICROPIGMENTACIÓN</P>
              <P class="p17 ft1">O MICROBLADING</P>
              <P class="p5 ft12">YO, DÑA <u>${data.consentInfo.name}</u>
              </P>
              <P class="p18 ft12">CON DNI N.º <u>${data.personalInfo.id}</u>
              </P>
              <P class="p18 ft12">Y FECHA DE NACIMIENTO <u>${data.personalInfo.dob}</u>
              </P>
              <P class="p19 ft12">AUTHORIZO AL TÉCNICO EN MICROPIGMENTACION REALIZARME UN TRATAMIENTO DE MICROPIGMENTACION EN:
              </P>
              <P class="p19 ft12">ZONDA DE <u>${data.consentInfo.zone1}</u>, FECHA <u>${data.consentInfo.date1}</u></P>
              <P class="p18 ft12">ZONDA DE <u>${data.consentInfo.zone2}</u>, FECHA <u>${data.consentInfo.date2}</u></P>
              <P class="p18 ft12">ZONDA DE <u>${data.consentInfo.zone3}</u>, FECHA <u>${data.consentInfo.date3}</u></P>
              <P class="p19 ft12">RECONOZCO QUE SE ME HA INFORMADO DE ESTE TRATAMIENTO Y SE ME HAN RESPONDIDO TODAS LAS
                  PREGUNTAS QUE ME HAN SURGIDO SOBRE ESTE TRATAMIENTO DE MICROPIGMENTACION.</P>
              <P class="p20 ft12">ASIMISMO, HE VISTO LA APARATOLOGÍA Y LOS PIGMENTOS CON LOS QUE SE ME VA A REALIZAR DICHO
                  SERVICIO Y ESTOY CONFORME TRAS HABERME MOSTRADO QUE ESTAN AUTORIZADOS POR EL MINISTERIO DE SANIDAD. SE ME
                  INFORMA QUE TODOS LOS SUPLIDOS PARA LA REALIZACIÓN DE LA MICROPIGMENTACIÓN QUE SE VAN A UTILIZAR SON
                  DESECHABLES, LOS CUALES SE ABREN Y DESECHAN EN MI PRESENCIA. EL TÉCNICO ESPECIALISTA ME HA REALIZADO UN
                  VISAGISMO (DISEÑO) PREVIO AL TRATAMIENTO, ESTANDO CONFORME CON EL MISMO, TANTO EN EL COLOR COMO EN LA FORMA.
              </P>
              <P class="p21 ft12">EXIMO DE TODA RESPONSABILIDAD A LA TÉCNICA EN MICROPIGMENTACIÓN AL CENTRO DE BELLEZA
              <u> ${data.consentInfo.center_name}</u> EN CASO DE SURGIR ALGUN PROBLEMA POR NO HABERLE</P>
              <P class="p22 ft12">INFORMADO SOBRE ALGUN ASPECTO DEL HISTORIAL CLÍNICO.</P>
              <P class="p23 ft12">AUTORIZO LA REALIZACIÓN DE FOTOGRAFIAS DEL ANTES Y EL DESPUÉS DE LA MICROPIGMENTACION
                  REALIZADA. LA CUSTODIA Y GESTIÓN DE DICHAS FOTOGRAFIAS ESTAN SUJETAS A LA LEY DE PROTECCION DE DATOS DE
                  CARÁCTER PERSONAL.</P>
              <P class="p24 ft12">AUTORIZO A DAR USO PROFESIONAL AL FOTOGRAFIADO QUE SE ME HACE DEL ANTES Y EL DESPUES DONDE
                  SE REQUIERA, COMO POR EJEMPLO EN PAGINA WEB EN INTERNET.</P>
              <P class="p25 ft12">SE ME INFORMA QUE LA MICRPIGMENTACION ES UNA TECNICA DE MICROPLANTACION DE PIGMENTOS EN LA
                  PIEL, CUYOS RESULTADOS SE IRÁN DESVANECIENDO GRADUALMENTE POR CAUSAS DIVERSAS COMO EL TONO APLICADO,
                  COSMETICA O CUIDADOS INAPROPRIADOS Y OTRAS PARTICULARIDADES, EN CONSECUENSIA, ACEPTO EL TRATAMIENTO DE
                  MICROPIGMENTACION VOLUNTARIAMENTE.</P>
              <P class="p26 ft12">RECONOZCO QUE ESTE TRATAMIENTO CONLLEVA UN PROCESO DE VARIOS RETOQUES Y QUE HASTA LA
                  FINALIZACION DE MISMO NO SE PUEDE REALIZAR UNA VALORACIÓN DEL TRABAJO, ES POR ESTE MOTIVO QUE ACEPTO QUE NO
                  DEVUELVE EL DINERO SI LLEGADO AL CASO, POR MOTIVOS INFUNDADAS NO ESTUVIERA DE ACUERDO CON EL TRABAJO SIN
                  HABER DEJADO EL TIEMPO OPORTUNO DE FINALIZACIÓN DEL TRATAMIENTO.</P>
              <P class="p27 ft12">DECLARO QUE SE ME HA DADO UNA HOJA INFORMATIVA ANTES DE REALIZARME LA MICROPIGMENTACIÓN, DE
                  LOS CUIDADOS ANTERIORES COMO MEDIDA PRVENTIVA EN LO QUE TIENE QUE VER, CON LA APARICION DE HERPES Y ASIMISMO
                  SE ME HACE ENTREGA DE LOS CUIDADOS POSTERIORES PARA LA PRONTA REGENRACIÓN DE LA PIEL Y ASUMO SEGUIR LOS
                  PASOS QUE SE ME SEÑALAN POR LO QUE ASUMO COMO MÍOS LOS PROBLEMAS DERIVADOS DE UN CUIDADO POST TRATAMIENTO DE
                  MICROPIGMENTACIÓN NO ADECUADO COMO CONSECUENCIA DE LAS PARTICULARIDADES DE MI CASO, EXIMIENDO A LA TÉCNIO
                  LILIT AVOYAN DE LOS INCONVENIENTES QUE PUEDAN SURGIR ASI COMO LOS PROBLEMAS QUE PUEDAN SURGIR POR NO HABERLE
                  INFORMADO SOBRE ALGUN ASPECTO DEL HISTORIAL CLÍNICO.</P>
              <P class="p28 ft12">ESTOY CONFORME CON EL TRATAMIENTO Y EL PRECIO GLOBAL PRESUPUESTADO, QUE INCLUYE DE
                  SENSIBILIDAD ALÉRGICA.</P>
              <P class="p22 ft12">DE <u>${data.consentInfo.from}</u> €</P>
              <P class="p19 ft12">Y PARA QUE CONSTE QUE ESTOY CONFORME Y EN PLENAS FACULTADES CON TODO LO ANTERIORMENTE
                  EXPUESTO FIRMO LA HOJA DE CONSENTIMIENTO Y RECONOCIMIENTO EN LA FECHA INDICADA.</P>
              <TABLE cellpadding=0 cellspacing=0 class="t0">    
                <TR>
                      <TD class="tr0 td0">
                          <P class="p29 ft12">FIRMA Y CONFORME CLIENTE:</P>
                      </TD>
                      <TD class="tr0 td1">
                          <P class="p29 ft13">FECHA: <u>${data.consentInfo.signdate}</u></P>
                      </TD>
                  </TR>
                  <TR><img src="${data.signature}" style='display:block; width:130px;height:80px;'/></TR>
              </TABLE>
          </DIV>
      </BODY>      
      </HTML>`,
      fileName: 'micro-blading-data-' + new Date().toString(),
      directory: 'Documents',
    };
    let file = await RNHTMLtoPDF.convert(options);
    console.log(file.filePath);
    return file.filePath;
  }
  return '';
};
